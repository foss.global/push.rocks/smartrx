// this file is intended to keep the bundle size down

export { Observable, Subject, fromEvent, ReplaySubject, Subscription } from 'rxjs';

export type { Observer } from 'rxjs';

import { startWith, takeUntil, map, debounce, debounceTime, throttleTime } from 'rxjs/operators';

export const ops = {
  debounce,
  debounceTime,
  map,
  startWith,
  takeUntil,
  throttleTime,
};
