// import test framework
import { expect, tap } from '@push.rocks/tapbundle';
import * as events from 'events';
import * as rx from 'rxjs';
import * as smartpromise from '@push.rocks/smartpromise';

// import the module
import * as smartrx from '../ts/index.js';

let testObservablemap: smartrx.Observablemap;
let testObservable1: rx.Observable<any>;
let testObservable2: rx.Observable<any>;
let testObservable3: rx.Observable<any>;
let testEmitter: events.EventEmitter;

tap.test('should create an instance', async () => {
  testObservablemap = new smartrx.Observablemap();
  expect(testObservablemap).toBeInstanceOf(smartrx.Observablemap);
});

tap.test('should accept a new emitter', async () => {
  let done = smartpromise.defer();
  testEmitter = new events.EventEmitter();
  testObservable1 = testObservablemap.getSubjectForEmitterEvent(testEmitter, 'event1');
  testObservable1.subscribe((x) => {
    done.resolve();
  });
  testObservable2 = testObservablemap.getSubjectForEmitterEvent(testEmitter, 'event1');
  testObservable3 = testObservablemap.getSubjectForEmitterEvent(testEmitter, 'event2');
  expect(testObservable1 === testObservable2).toBeTrue();
  expect(testObservable1 === testObservable3).toBeFalse();
  testEmitter.emit('event1');
  await done.promise;
});

tap.start();
